vector = Array{Int64, 1}()
println("Тип этой штуки? Array{Int64, 1}() -> ", typeof(vector))

matrix = Array{Int64, 2}[]
println("Тип этой штуки? Array{Int64, 2}[] -> ", typeof(matrix))


row = [1 2 3]
column = [1, 2, 3]
println("Одинаковые вектора? ", row == column)


not_over_9000 = zeros(300, 30)
println("Там кол? ", isone(not_over_9000[2,20]))
println("Там нуль? ", iszero(not_over_9000[2,20]))
println("Размер? ", size(not_over_9000[2,20]))
println("А этот размер? ", size(not_over_9000))
println("А длина? ", length(not_over_9000))

# ?typeof