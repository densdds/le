function increment_int(input_int::Int64)::Int64
    return input_int + 1
end

function increment(input)
    input + 1
end

increment_math_like(x) = x + 1

increment_lambda = x -> x + 1

print("Конвейер: ")
1 |> increment_int |> increment |> increment_math_like |> increment_lambda |> println

nine = [x for x in 1:9]
println(nine, " <- vector | incremented -> ", increment_int.(nine))

nine[4:6] .= 0
nine |> println

function example_function(a, b=0, args...; c, d=1, kwargs...)
    println(a)
    println(b)
    println(args)
    println(c)
    println(d)
    println(kwargs)
end

println(example_function('a', 2, 3, 4; c=3, e=7))
println(example_function(1; c=7))

f(a, b, c) = a + b + c
x = (1, 2, 3)
println(f(x...))


a = [3, 2, 1]
"a: " * string(a) |> println
b = sort(a)
println("a: ", a, " b: ", b)
sort!(a)
"a: $a" |> println