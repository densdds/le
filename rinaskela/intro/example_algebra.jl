println("3 ≠ 2 ? ", 3 ≠ 2)

rational = 12 // 2
println("""Тип числа $rational -- $typeof(rational)""")

первое_число = rand()
второе_число = rand()
result_string = первое_число < второе_число ? "Первое" : "Второе"
result_string *= " число оказалось больше"
println(result_string)

println("Корень уравнения 2x=6: ", 2 \ 6)