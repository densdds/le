function add_element_to(collection::Vector, element)::Array{Any}
    array_to_return = Vector{Any}()
    push!(array_to_return, collection...)
    push!(array_to_return, element)
    return array_to_return
end


function add_element_to(collection::Dict; key, value)::Dict
    dict_to_return = Dict{Any, Any}()
    dict_to_return = merge(dict_to_return, collection)
    dict_to_return[key] = value
    _print_dict_keys(dict_to_return)
    return dict_to_return
end


function _print_dict_keys(dict_to_return)
    dict_to_return |> keys .|> println
end


function add_element_to(collection::Tuple, element)::Tuple
    tuple_to_return = tuple(collection..., element)
    return tuple_to_return
end


function add_element_to(collection::NamedTuple; name, value)::NamedTuple end


add_element_to([1, 2, 3], "4") |> println
add_element_to(
    Dict(
        1 => "one",
        2 => "two",
        3 => "three"
    );
    key="four",
    value=4,
) |> println
add_element_to((0.0, "hello", 6*7), 55555) |> println
add_element_to((0, 1, 2), "five") |> println
